# Unpublished Paragraphs

This module controls the visibility of unpublished paragraphs for authenticated users with the right permission.

## Requirements

This module requires the [Paragraphs module](https://www.drupal.org/project/paragraphs).

## Installation

Same as any normal module.  From the command line:

```$bash
composer require drupal/unpublished_paragraphs
drush en unpublished_paragraphs
```

## Usage

1. Install and enable the module (see instructions above)
1. Edit a piece of content with Paragraphs
1. Un-check the "Published" button on one of your Paragraphs
1. Save and view the node
1. You should see a button that says "Toggle visibility of unpublished items" in the lower right corner of the page.
1. Click the button to make unpublished Paragraphs appear and disappear.
